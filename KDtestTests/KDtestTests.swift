//
//  KDtestTests.swift
//  KDtestTests
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import XCTest
@testable import KDtest

class KDtestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {

        super.tearDown()
    }
    
    let a = 54
    let b = 24
    let expResult = 6
    
    func testExample() {
        let result = gcd(a: a, b: b)
        XCTAssert(result == expResult)
    }
    
    func testLCM() {
        let a = [10,5,2]
        XCTAssert(try lcm(arr: a) == 10)
    }
    
    func testPetNames() {
        let me = Human(name: "Mk Tan")
        me.adopt(pet: Pet(name: "Dolly"))
        me.adopt(pet: Pet(name: "Bob"))
        me.adopt(pet: Pet(name: "Lucky"))
        let petNames = me.petNames()
        let expPetNames = "Dolly,Bob,Lucky"
        XCTAssert(petNames == expPetNames)
    }
    
    let manager = CreditManager()
    
    func testConstructor() {
        XCTAssertEqual(manager.balance, 0)
    }
    
    func testAddChangesBalance() {
        let entry = Entry(credits: 5)
        let _  = manager.add(entry: entry)
        XCTAssertEqual(manager.balance, 5)
    }
    
    func testAddReturnsTuple() {
        let entry = Entry(credits: 5)
        XCTAssertTrue(manager.add(entry: entry) == (true, 5))
    }
    
    func testDeductChangesBalance() {
        manager.balance = 11
        let entry = Entry(credits: 5)
        let _  = manager.deduct(entry: entry)
        XCTAssertEqual(manager.balance, 6)
    }
    
    func testDeductReturnsTuple() {
        manager.balance = 11
        let entry = Entry(credits: 5)
        XCTAssertTrue(manager.deduct(entry: entry) == (true, 6))
    }
    
    func testDeductReturnsFalseAndCurrentBalanceForNegativeBalance(){
        manager.balance = 5
        let entry = Entry(credits: 6)
        XCTAssertTrue(manager.deduct(entry: entry) == (false, 5))
    }
    
    func testOperateWithDeductCredit() {
        manager.balance = 0
        let entry = Entry(credits: -6)
        XCTAssertTrue(manager.operate(entry: entry) == (false, 0))
    }
    
    func testOperateWithAddCredit() {
        manager.balance = 0
        let entry = Entry(credits: 4)
        XCTAssertTrue(manager.operate(entry: entry) == (true, 4))
    }
}
