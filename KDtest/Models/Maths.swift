//
//  Calculator.swift
//  KDtest
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import Foundation

enum GenericError: Error {
    case containsZero
    case invalidParameter
}

func lcm(arr:[Int]) throws -> Int {
    var factors = [Int: Int]()
    for n in arr.map({ abs($0) }) {
        let primeFactorization = primeFactorizationOf(n: n)
        primeFactorization.forEach { if (factors[$0] ?? 0) < $1 { factors[$0] = $1 }}
    }
    let res = Int(factors.reduce(1.0) { $0 * pow(Double($1.0), Double($1.1)) })
    return res
    
}

func gcd(a:Int, b:Int) -> Int {
    if a == b {
        return a
    }
    else {
        if a > b {
            return gcd(a:a-b,b:b)
        }
        else {
            return gcd(a:a,b:b-a)
        }
    }
}

public func primeFactorizationOf(n: Int) -> [Int: Int] {
    guard n > 1 else { return [:] }
    
    let primeNumbers = primeNumbersUpTo(n: n)
    var factors = [Int: Int]()
    var m = n
    
    while m != 1 {
        for prime in primeNumbers {
            if m % prime == 0 {
                factors[prime] = (factors[prime] ?? 0) + 1
                m /= prime
                break
            }
        }
    }
    
    return factors
}

public func primeNumbersUpTo(n: Int) -> [Int] {
    guard n > 1 else { return [] }
    
    // First we build this array [0, 0, 2, 3, 4, 5, 6, 7, 8, 9, ..., n].
    var array = Array(repeating: 0, count: n + 1)
    for i in 2...n {
        array[i] = i
    }
    
    // Then we use the Sieve of Eratosthenes method to get [0, 0, 2, 3, 0, 5, 0, 7, 0, 0, ..., n or 0].
    for i in 2...n {
        if array[i] != 0 {
            var j = i * 2
            while j <= n {
                array[j] = 0
                j += i
            }
        }
    }
    
    return array.filter{ $0 != 0 }
}



