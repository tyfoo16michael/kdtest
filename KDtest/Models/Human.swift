//
//  Q2.swift
//  KDtest
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import Foundation

class Pet {
    let name: String
    var owner: Human?
    init(name: String){
        self.name = name
    }
}

class Human {
    var name: String
    var pets: [Pet] = []
    
    lazy var petNames: () -> String = {
        let res = (self.pets.map{$0.name}).joined(separator: ",")
        return res
    }
    
    init(name: String){
        self.name = name
    }
    
    func adopt(pet: Pet){
        pets.append(pet)
        pet.owner = self
    }
}
