//
//  CreditManager.swift
//  KDtest
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import Foundation

class Entry {
    let description: String
    let credits: Int
    
    init(credits: Int, description: String = "") {
        self.credits = credits
        self.description = description
    }
}

class CreditManager {
    var balance: Int
    
    init(balance: Int = 0) {
        self.balance = balance
    }
    
    func add(entry: Entry) -> (result: Bool, balance: Int) {
        print("Used add.")
        let credit = entry.credits
        self.balance += credit
        return (true, self.balance)
    }
    
    func deduct(entry: Entry) -> (result: Bool, balance: Int){
        let credit = entry.credits < 0 ? (entry.credits * -1) : entry.credits
//        let _balance = self.balance
        if self.balance-credit <= 0 {
            return (false, self.balance)
        }
        self.balance -= credit
        return (true, self.balance)
    }
    
    func operate(entry: Entry) -> (result: Bool, balance: Int){
        let credit = entry.credits
        if credit < 0 {
            return deduct(entry:entry)
        }else{
            return add(entry:entry)
        }
    }
    
}
