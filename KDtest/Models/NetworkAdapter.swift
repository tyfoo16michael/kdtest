//
//  NetworkAdapter.swift
//  KDtest
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import Foundation

struct User {
    let name : String
    let avatarUrl: String
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

protocol ImageUpdateDelegate: class {
    func updateImage(_ url: String)
}

class NetworkAdapter: NSObject {
    
    typealias Completion = (Result<Any>) -> Void
    let url = "https://www.google.com"
    let session = URLSession.shared
    weak var delegate: ImageUpdateDelegate?
    var lastImageUrl: String = ""
    
    func getAuthToken(email: String, password: String, completion: @escaping Completion) {
        
        if email.isEmpty || password.isEmpty {
            completion(.failure(GenericError.invalidParameter))
        }
        
        session.dataTask(with: URL(string: url)!) { (data, res, err) in
            if err != nil {
                completion(.failure(err!))
                return
            }
            if res != nil {
                if let httpRes = res as? HTTPURLResponse {
                    if httpRes.statusCode == 200 {
                        let accessToken = "a12345678890z"
                        completion(.success(accessToken))
                        return
                    }
                }
            }
        }.resume()
    }
    
    func getUserProfile(token: String, completion: @escaping Completion) {
        
        let randomInt = Int(arc4random_uniform(2))
        let urls = [
            "https://image.flaticon.com/teams/slug/google.jpg",
            "https://cdn1.iconfinder.com/data/icons/appicns/128/appicns_Firefox.png"
        ]
        let user = User(name: "foo", avatarUrl: urls[randomInt])

        session.dataTask(with: URL(string: url)!) { (data, res, err) in
            if err != nil {
                completion(.failure(err!))
                return
            }
            if res != nil {
                if let httpRes = res as? HTTPURLResponse {
                    if httpRes.statusCode == 200 {
                        completion(.success(user))
                        return
                    }
                }
            }else{
                let error = NSError(domain: "", code: -1, userInfo: ["localizeDescription": "No Data return"])
                completion(.failure(error))
            }
        }.resume()
    }
    
    func checkIfUserAvatarChanged(user: User) {
        let urlChanged = lastImageUrl != user.avatarUrl ? true: false
        if urlChanged {
            lastImageUrl = user.avatarUrl
            delegate?.updateImage(user.avatarUrl)
        }
    }
}


