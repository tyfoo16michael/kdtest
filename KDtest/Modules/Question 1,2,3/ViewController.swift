//
//  ViewController.swift
//  KDtest
//
//  Created by tyfoo on 10/04/2018.
//  Copyright © 2018 foo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    
    let adapter = NetworkAdapter.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.delegate = self
        self.question1()
        self.question2()
        onRefreshTapped()
        refreshButton.addTarget(self, action: #selector(onRefreshTapped), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func question1() {
        let a = [10,5,2]
        do {
            let answer = try lcm(arr: a)
            print(#function, answer)
        } catch (let error) {
            print(error)
        }
    }
    
    func question2() {
        let me = Human(name: "Mk Tan")
        me.adopt(pet: Pet(name: "Dolly"))
        me.adopt(pet: Pet(name: "Bob"))
        me.adopt(pet: Pet(name: "Lucky"))
        print(#function, me.petNames())
    }
    
    @objc func onRefreshTapped() {
        let email = "someone@gmail.com"
        let password = "21321313"
        self.refreshButton.isEnabled = false
        adapter.getAuthToken(email: email, password: password) { (result) in
            switch result {
            case .success(let token):
                if let _token = token as? String {
                    self.adapter.getUserProfile(token: _token, completion: { (userResult) in
                        switch userResult {
                        case .success(let user):
                            DispatchQueue.main.async {
                                self.refreshButton.isEnabled = true
                            }
                            self.adapter.checkIfUserAvatarChanged(user: user as! User)
                            break
                        case .failure(let error):
                            self.showAlert(title: "Error", message: error.localizedDescription)
                            break
                        }
                    })
                }
                break
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
                break
            }
        }
    }

}
extension ViewController: ImageUpdateDelegate {
    func updateImage(_ url: String) {
        self.imageView.imageFromServerURL(urlString: url)
        self.showAlert(title: "", message: "Image Updated")
    }
}

